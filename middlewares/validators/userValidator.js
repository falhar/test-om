const {
  check,
  validationResult,
  sanitize,
  matchedData
} = require('express-validator')
const {
  user
} = require('../../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const path = require('path')
const multer = require('multer')
const crypto = require('crypto')

const uploadDir = '/img/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {
  signup: [
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_\-]{3,32}$/)
    .withMessage('invalid username value'),
    check('gender').notEmpty().withMessage('gender cant null').custom(value => {
      if (value.toLowerCase() == 'male') {
        return true
      } else if (value.toLowerCase() == 'female') {
        return true
      } else {
        throw new Error("input 'male' or 'female'")
      }
    }),
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail().withMessage('email must be email address'),
    check('password').notEmpty().withMessage('password cant null').isLength({
      min: 6,
      max: 32
    }).withMessage('password must have 6 to 32 characters'),
    check('passwordConfirmation')
    .notEmpty().withMessage('password confirmation cant null').custom((value, {
      req
    }) => value === req.body.password).withMessage('wrong password confirmation'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  //Validation check when user login
  login: [
    check('emailOrUsername').notEmpty().withMessage('email or username cant null')
    .isString().withMessage('input must be username or email address'),
    check('password').notEmpty().withMessage('password cant null'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  //show other user profile
  getOne: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value,
          role: "user"
        }
      }).then(result => {
        if (!result) {
          throw new Error('user not found!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  //Validation check when user update profile
  update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.size > 1 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 1mb'),
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_\-]{3,32}$/)
    .withMessage('invalid username value'),
    check('gender').notEmpty().withMessage('gender cant null').custom(value => {
      if (value.toLowerCase() == 'male') {
        return true
      } else if (value.toLowerCase() == 'female') {
        return true
      } else {
        throw new Error("input 'male' or 'female'")
      }
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  admin_update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.size > 1 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 1mb'),
    check('username').notEmpty().withMessage('username cant null').matches(/^[a-zA-Z0-9\_]{3,32}$/)
    .withMessage('username must have 3 to 32 characters without any space'),
    check('fullName').isString().matches(/^[a-zA-Z\ ']{0,32}$/)
    .withMessage('invalid full name value'),
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail()
    .withMessage('Input must be email address')
    .custom(value => {
      return user.findOne({
        where: {
          email: value
        }
      }).then(result => {
        if (result) {
          return false
        }
        return true
      })
    }).withMessage('email already used'),
    check('address').isString().withMessage('must be an address'),
    check('role').toLowerCase().isAlpha()
    .withMessage('role is only user or admin'),
    check('isVerified').isBoolean(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  delete: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value
        },
        paranoid: false
      }).then(result => {
        if (!result) {
          throw new Error('User not found!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  restore: [
    check('username').custom(value => {
      return user.findOne({
        where: {
          username: value
        },
        paranoid: false
      }).then(result => {
        if (!result) {
          throw new Error('User not found!')
        } else if (result.deletedAt = null) {
          throw new Error('User exists, no need to restore')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ]
}
