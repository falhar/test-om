const {
  product,
  user
} = require('../../models')
const {
  matchedData,
  sanitize,
  validationResult,
  check
} = require('express-validator')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/'
const storage = multer.diskStorage({
  destination: './public' + uploadDir,
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {

  getOne: [
    check('id').custom(async value => {
      const theProduct = await product.findOne({
        where: {
          id: value
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      if (!theProduct) {
        throw new Error("Product ID doesn't exist!")
      } else {
        return true
      }
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  getByName: [
    check('keyword').custom(value => {
      return product.findAll({
        where: {
          name: {
            [Op.like]:"%"+value+"%"
          }
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      }).then(result => {
        if (result.length == 0) {
          throw new Error("No product matched!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  getBySeller: [
    check('username').custom(async value => {
      const seller = await user.findOne({
        where: {
          username: value
        }
      })
      return product.findAll({
        where: {
          sellerId: seller.id
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Seller doesn't have product")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],

  create: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('name').isString().notEmpty().withMessage('product name cant null'),
    check('desc').isString(),
    check('price').isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('name').isString().notEmpty().withMessage('product name cant null'),
    check('desc').isString(),
    check('price').isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return product.findOne({
        where: {
          id: value
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      }).then(result => {
        if (result.length == 0) {
          throw new Error("Product ID doesn't exist!")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
