const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes')

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cors())

app.use(express.static('public'));

app.use('/', userRoutes);
app.use('/product', productRoutes)

const port = process.env.PORT || 3000

app.listen(port, ()=>{
  console.log(`program running on port ${port}`);
})
