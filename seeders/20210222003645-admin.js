'use strict';
const bcrypt = require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user', [{
      username: "admin",
      fullName: "admin falhar",
      image: "db23a4ce3ce287ec2b2591963f956cff.png",
      gender: "male",
      email: "falhar@admin.com",
      role: "admin",
      password: bcrypt.hashSync("123456", 12),
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: null
    }])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {})
  }
};
