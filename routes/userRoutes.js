const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../middlewares/auth');
const userValidator = require('../middlewares/validators/userValidator');
const UserController = require('../controllers/userController');

//Register
router.post('/signup', [userValidator.signup, function(req, res, next) {
  passport.authenticate('signup', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.signup(user, req, res, next);
  })(req, res, next);
}]);

//Login
router.post('/login', [userValidator.login, function(req, res, next) {
  passport.authenticate('login', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.login(user, req, res, next);
  })(req, res, next);
}]);

//Show Our Profile
router.get('/profile', [passport.authenticate('user', {
  session: false
})], UserController.getOne)

//Update Profile
router.put('/profile/update', [userValidator.update, passport.authenticate('user', {
  session: false
})], UserController.update)

//Show other user profile
router.get('/profile/:username', userValidator.getOne, UserController.getOneOther)

//Delete User via admin
router.delete('/delete/:username', [userValidator.delete, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.delete(user, req, res, next);
  })(req, res, next);
}])

//Restore User via admin
router.put('/restore/:username', [userValidator.restore, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.restore(user, req, res, next);
  })(req, res, next);
}])

//Hard delete via admin
router.delete('/hard_delete/:username', [userValidator.delete, function(req, res, next) {
  passport.authenticate('admin', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.hard_deleted(user, req, res, next);
  })(req, res, next);
}])

module.exports = router
