const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../middlewares/auth');
const productValidator = require('../middlewares/validators/productValidator');
const ProductController = require('../controllers/productController');

//get all product
router.get('/', ProductController.getAll)

//get product by id product
router.get('/:id', productValidator.getOne, ProductController.getOne)

//get product by seller
router.get('/seller/:username', productValidator.getBySeller, ProductController.getBySeller)

//get product by keyword(search product)
router.get('/find/:keyword', productValidator.getByName, ProductController.getByName)

//create product
router.post('/create/', [productValidator.create, passport.authenticate('user', {
  session: false
})], ProductController.create)

//update product
router.put('/update/:id', [productValidator.update, passport.authenticate('user', {
  session: false
})], ProductController.update)

//delete product
router.delete('/delete/:id', [productValidator.delete, passport.authenticate('user', {
  session: false
})], ProductController.delete)

module.exports = router
