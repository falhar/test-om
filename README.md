# Test

API Documentation : [Link](https://documenter.getpostman.com/view/13706161/TWDXobxg)

How to run :

```bash
git clone https://gitlab.com/falhar/test-om.git
```
```bash
git pull origin master
```
```bash
npm install
```
```bash
sequelize db:create
```
```bash
sequelize db:migrate
```
```bash
sequelize db:seed:all
```
```bash
npm test
```
