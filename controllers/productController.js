const {product, user} = require('../models')
const {check, validationResult, matchedData, sanitize} = require('express-validator')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

class ProductController {

  async getAll(req, res) {
    try {
      const result = await product.findAll({
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      res.json({
        status: "Success",
        data: result
      })
    } catch (e) {
      res.json({
        status: "Error",
        error: e
      })
    }
  }

  async getBySeller(req, res) {
    try {
      const seller = await user.findOne({
        where: {
          username: req.params.username
        }
      })
      const result = await product.findAll({
        where: {
          sellerId: seller.id
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      res.json({
        staus: "Success",
        data: result
      })
    } catch (e) {
      res.json({
        status: "Error",
        error: e
      })
    }
  }


  async getByName(req, res) {
    try {
      const result = await product.findAll({
        where: {
          name: {
            [Op.like]:"%"+req.params.keyword+"%"
          }
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      res.json({
        status: "Success",
        data: result
      })
    } catch (e) {
      res.json({
        status: "Error",
        error: e
      })
    }
  }

  async getOne(req, res) {
    try {
      product.findOne({
        where: {
          id: req.params.id
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      }).then(result => {
        res.json({
          status: "Success",
          data: result
        })
      })
    } catch (e) {
      res.json({
        status: "Error",
        error: e
      })
    }
  }

  async create(req, res) {
    try {
      product.create({
        name: req.body.name,
        price: req.body.price,
        desc: req.body.desc,
        sellerId: req.user.id,
        image: req.file.filename,
      }).then(result => {
        res.json({
          status: "Succcess add product",
          data: result
        })
      })
    } catch (e) {
      res.json({
        status: "Error",
        error: e
      })
    }
  }

  async update(req, res) {
    try {
      const prevData = await product.findOne({
        where: {
          id: req.params.id
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      await product.update({
        name: req.body.name,
        price: req.body.price,
        desc: req.body.desc,
        sellerId: req.user.id,
        image: req.file === undefined ? prevData.image.substr(5) : req.file.filename,
      }, {
        where: {
          id: req.params.id
        }
      })
      const newData = await product.findOne({
        where:{
          id:req.params.id
        },
        attributes: ['id', 'name', 'price', 'image', 'desc'],
        include: [{
          model: user,
          attributes: ['id', 'username', 'fullName', 'image'],
          as: 'seller'
        }]
      })
      res.json({
        status: "Success update product!",
        data: newData
      })
    } catch (e) {
      console.log(e);
      res.json({
        status: "Error",
        error: e
      })
    }
  }

  async delete(req, res) {
    product.destroy({
        where:{
          id: req.params.id
        }
      }).then(async (result) => {
        res.json({
          status: "Success delete product!",
          data: null
        })
      })
  }
}

module.exports = new ProductController
