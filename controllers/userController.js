const {
  user
} = require('../models');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

class UserController {
  async signup(user, req, res) {
    const body = {
      username: user.username,
      id: user.id
    }

    const token = jwt.sign({
      user: body
    }, 'hashPassword')

    res.status(200).json({
      message: "Signup success!",
      token: token
    })
  }

  async login(user, req, res) {
    const body = {
      username: user.username,
      id: user.id
    }

    const token = jwt.sign({
      user: body
    }, 'hashPassword')

    res.status(200).json({
      message: "Login success!",
      token: token
    })
  }

  async getOne(req, res) {
    try {
      const result = await user.findOne({
        where: {
          id: req.user.id
        }
      })

      res.status(200).json({
        status: "Success get data!",
        data: result
      })
    } catch (e) {
      res.status(422).json({
        status: "Error!",
        error: e
      })
    }
  }

  async getOneOther(req, res) {
    try {
      const result = await user.findOne({
        where: {
          username: req.params.username
        },
        attributes: ['username', 'fullName', 'gender', 'image', ]
      })
      res.status(200).json({
        status: "Success!",
        data: result
      })
    } catch (e) {
      res.status(422).json({
        status: "Error!",
        error: e
      })
    }
  }

  async update(req, res) {
    try {
      const prevUser = await user.findOne({
        where: {
          id: req.user.id
        }
      })
      await user.update({
        username: req.body.username,
        image: req.file === undefined ? prevUser.image.substr(5) : req.file.filename,
        gender: req.body.gender,
        fullName: req.body.fullName
      }, {
        where: {
          id: req.user.id
        }
      })
      const newUser = await user.findOne({
        where: {
          id: req.user.id
        }
      })
      res.status(200).json({
        status: "Update Success",
        data: newUser
      })
    } catch (e) {
      if (e.code="ER_DUP_ENTRY") {
        res.status(422).json({
          status: "Error",
          message: "Username already used!"
        })
      }
      res.status(422).json({
        status: "Error",
        error: e
      })
    }
  }

  async delete(token, req, res) {
    try {
      user.destroy({
        where: {
          username: req.params.username
        }
      })
      res.status(200).json({
        status: "Delete Success",
        message: "User deleted"
      })
    } catch (e) {
      res.status(422).json({
        status: "Error",
        error: e
      })
    }
  }

  async restore(token, req, res) {
    try {
      await user.restore({
        where: {
          username: req.params.username
        }
      })
      const result = await user.findOne({
        where: {
          username: req.params.username
        }
      })
      res.status(200).json({
        status: "Restore Success",
        data: result
      })
    } catch (e) {
      res.status(422).json({
        status: "Error",
        error: e
      })
    }
  }

  async hard_deleted(token, req, res) {
    try {
      user.destroy({
        where: {
          username: req.params.username
        },
        force: true
      })
      res.status(200).json({
        status: "Success",
        message: "User deleted"
      })
    } catch (e) {
      res.status(422).json({
        status: "Error",
        error: e
      })
    }
  }
}

module.exports = new UserController
